import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import './assets/scss/main.scss';

ReactDOM.render(<App />, document.getElementById('root'));
